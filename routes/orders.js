const express = require('express');
const router = express.Router();
const orders = require('../models/orders');
const validateIndex = require('../middleware/orders');
const idAdmin = require('../middleware/admin');

/**
 * @swagger
 *   /orders/admin:
 *    post:
 *      tags: [Orders]
 *      description: show all orders
 *      parameters:
 *       - name: idAdmin
 *         description: id of admin
 *         in: formData
 *         required: true
 *         type: string
 *      responses:
 *        200: 
 *          description: Success
 */
 router.post('/admin', idAdmin, function (req, res) {
    res.send(orders);
});
/**
 * @swagger
 *   /orders/users:
 *    post:
 *      tags: [Orders]
 *      description: show orders for id
 *      parameters:
 *       - name: idUsers
 *         description: id Users
 *         in: formData
 *         required: true
 *         type: string
 *      responses:
 *        200: 
 *          description: Success
 */
 router.post('/users', function (req, res) {
    const resultado = orders.find (user => user.idUsers === req.body.idUsers)
    res.send(resultado);
});

/**
 * @swagger
 *   /orders:
 *    post:
 *      tags: [Orders]
 *      description: create orders
 *      parameters:
 *       - name: idProduct
 *         description: id of Product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: idUsers
 *         description: id of user
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: idPayMethod
 *         description: id of pay method
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: quantity
 *         description: quantity of product
 *         in: formData
 *         required: true
 *         type: integer
 *      responses:
 *        200:
 *          description: Success
 * 
 */
router.post('/', function (req, res) {
    const { idProduct, idUsers, idPayMethod, quantity }= req.body;
    if (idProduct && idUsers && idPayMethod && quantity) {
    const idOrder = orders.length + 1;
    let state = "new";
    date = new Date();
    time = `${date.getHours()}:${date.getMinutes()}`;
    let newOrder = {...req.body, idOrder, state, quantity, time};
    orders.push(newOrder);
    res.send('order created')
    } else {
    res.send('incomplete data');
    }
});
/**
 * @swagger
 *   /orders/admin:
 *    put:
 *      tags: [Orders]
 *      description: actualized order for admin 
 *      parameters:
 *       - name: idAdmin
 *         description: Id of admin 
 *         in: formData
 *         required: true
 *         type: string
 *       - name: indice
 *         description: Index of order actualized 
 *         in: formData
 *         required: true
 *         type: string
 *       - name: idProduct
 *         description: id of Product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: idUsers
 *         description: id of user
 *         in: formData
 *         required: true
 *         type: string
 *       - name: idPayMethod
 *         description: id of pay method
 *         in: formData
 *         required: false
 *         type: string
 *       - name: quantity
 *         description: quantity of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: state
 *         description: state of order
 *         in: formData
 *         required: true
 *         type: string
 *      responses:
 *        200:
 *          description: Success
 * 
 */
 router.put('/', validateIndex, idAdmin, function (req, res) {
    orders[req.body.indice].idProduct = req.body.idProduct;
    orders[req.body.indice].idUsers = req.body.idUsers;
    orders[req.body.indice].idPayMethod = req.body.idPayMethod;
    orders[req.body.indice].state = req.body.state;
    orders[req.body.indice].quantity = req.body.quantity;
    res.send('actualized order')
});
/**
 * @swagger
 *   /orders/admin:
 *    delete:
 *      tags: [Orders]
 *      description: delete orders
 *      parameters:
 *      - name: indice
 *        description: index delete order
 *        in: formData
 *        requider: true
 *        type: string
 *      responses:
 *        200:
 *         description: Success
 */
 router.delete('/admin', validateIndex, function (req, res) {
    orders.splice(req.body.indice,1);
    res.send(orders);
});

module.exports = router;