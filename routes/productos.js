const express = require('express');
const router = express.Router();
const productos = require('../models/productos');
const validateIndex = require('../middleware/productos');
const idAdmin = require('../middleware/admin');
/**
 * @swagger
 *   /productos:
 *    get: 
 *      tags: [Products]
 *      description: All Products
 *      responses:
 *        200: 
 *          description: Success
 */
  router.get('/', (req, res) => {
     res.json(productos);
  });
  
/**
 * @swagger
 *   /productos/admin:
 *    post:
 *      tags: [Products]
 *      description: create Product
 *      parameters:
 *       - name: idAdmin
 *         description: id of admin
 *         in: formData
 *         required: true
 *         type: string
 *       - name: nombre
 *         description: name of Product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: precio
 *         description: Price of product
 *         in: formData
 *         required: true
 *         type: string
 *      responses:
 *        200:
 *          description: Success
 * 
 */
 router.post('/admin', idAdmin, (req, res) => {
  const { nombre, precio }= req.body;
  if (nombre && precio) {
    const idProduct = productos.length + 1;
    const newProduct = {nombre, precio, idProduct};
    productos.push(newProduct);
    res.json(productos);
  } else {
    res.json('incomplete data');
  }
});
/**
* @swagger
*   /productos/admin:
*    put:
*      tags: [Products]
*      description: actualized Product
*      parameters:
*       - name: idAdmin
*         description: Id of admin
*         in: formData
*         required: true
*         type: string
*       - name: indice
*         description: Index of product actualized 
*         in: formData
*         required: true
*         type: string
*       - name: nombre
*         description: name of Product
*         in: formData
*         required: true
*         type: string
*       - name: precio
*         description: Price of product
*         in: formData
*         required: true
*         type: string
*      responses:
*        200:
*          description: Success
* 
*/
 router.put('/admin', validateIndex, idAdmin, function (req, res) {
   productos[req.body.indice].nombre = req.body.nombre;
   productos[req.body.indice].precio = req.body.precio;
   res.send('actualized product')
 })
 /**
* @swagger
*   /productos/admin:
*    delete:
*      tags: [Products]
*      description: delete product
*      parameters:
*      - name: idAdmin
*        description: id of admin
*        in: formData
*        requider: true
*        type: string
*      - name: indice
*        description: index delete product
*        in: formData
*        requider: true
*        type: string
*      responses:
*        200:
*         description: Success
*/
router.delete('/admin', validateIndex, idAdmin, function (req, res) {
  productos.splice(req.body.indice,1);
  res.send('delete product');
})

module.exports = router;