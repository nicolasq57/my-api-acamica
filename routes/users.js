const express = require('express');
const router = express.Router();
const users = require('../models/users');
const validateIndex = require('../middleware/users');
const funcU = require('../controllers/users');
/**
 * @swagger
 *   /users/admin:
 *    get: 
 *      tags: [users]
 *      description: All user register
 *      responses:
 *        200: 
 *          description: Success
 */
router.get('/admin', (req, res) => {
res.json(users);
});
/**
 * @swagger
 *   /users:
 *    post:
 *      tags: [users]
 *      description: create users
 *      parameters:
 *       - name: username
 *         description: name of user
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: AccessPassword
 *         in: formData
 *         required: true
 *         type: string
 *       - name: name
 *         description: realname 
 *         in: formData
 *         required: true
 *         type: string
 *       - name: lastname
 *         description: real lastname
 *         in: formData
 *         required: true
 *         type: string
 *       - name: email
 *         description: emailOfContact
 *         in: formData
 *         required: true
 *         type: string
 *       - name: address
 *         description: adressOrder
 *         in: formData
 *         required: true
 *         type: string
 *       - name: phone
 *         description: phoneContact
 *         in: formData
 *         required: true
 *         type: string
 *      responses:
 *        200:
 *          description: Success
 * 
 */
router.post('/', function (req, res){
      res.json(funcU.newUser(req.body))
});
/**
 * @swagger
 *   /users:
 *    put:
 *      tags: [users]
 *      description: actualized users
 *      parameters:
 *       - name: indice
 *         description: index user actulized
 *         in: formData
 *         requider: true
 *         type: string
 *       - name: username
 *         description: name of user
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: AccessPassword
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: name
 *         description: realname 
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: lastname
 *         description: real lastname
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: email
 *         description: emailOfContact
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: adress
 *         description: adressOrder
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: phone
 *         description: phoneContact
 *         in: formData
 *         required: true
 *         type: integer
 *      responses:
 *        200:
 *          description: Success
 * 
 */
router.put('/', function (req, res) {
  users[req.body.indice].username = req.body.username;
  users[req.body.indice].password = req.body.password;
  users[req.body.indice].phone = req.body.phone;
  users[req.body.indice].email = req.body.email;
  users[req.body.indice].name = req.body.name;
  users[req.body.indice].lastname = req.body.lastname;
  users[req.body.indice].adress = req.body.adress;

  res.send('actualized users')
})
/**
 * @swagger
 *   /users/admin:
 *    delete:
 *      tags: [users]
 *      description: delete users
 *      parameters:
 *      - name: indice
 *        description: index delete user
 *        in: formData
 *        requider: true
 *        type: string
 *      responses:
 *        200:
 *         description: Success
 */
router.delete('/admin', validateIndex, function (req, res) {
  users.splice(req.body.indice,1);
  res.send('eliminated users');
})
/**
 * @swagger
 *   /users/login:
 *    post:
 *      tags: [users login]
 *      description: login users
 *      parameters:
 *      - name: username
 *        description: name of user or email
 *        in: formData
 *        requider: true
 *        type: string
 *      - name: password
 *        description: password
 *        in: formData
 *        requider: true
 *        type: string
 *      responses:
 *        200:
 *         description: Success
 */
router.post('/login', (req, res) => {
  res.json(funcU.login(req.body.username, req.body.password));
});  

module.exports = router;