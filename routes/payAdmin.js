const express = require('express');
const router = express.Router();
const validateIndex = require('../middleware/pay'); 
const pay = require('../models/pay');
const idAdmin = require('../middleware/admin');
/**
 * @swagger
 *   /pay/showAdmin:
 *    post:
 *      tags: [Pay]
 *      description: show pay method
 *      parameters:
 *       - name: idAdmin
 *         description: id of admin
 *         in: formData
 *         required: true
 *         type: string
 *      responses:
 *        200: 
 *          description: Success
 * 
 */
router.post('/showAdmin', idAdmin, (req, res) => {
res.json(pay);
});
/**
 * @swagger
 *   /pay/admin:
 *    post:
 *      tags: [Pay]
 *      description: create pay method
 *      parameters:
 *       - name: idAdmin
 *         description: id of admin
 *         in: formData
 *         required: true
 *         type: string
 *       - name: payMethod
 *         description: pay method
 *         in: formData
 *         required: true
 *         type: string
 *      responses:
 *        200:
 *          description: Success
 * 
 */
router.post('/admin', idAdmin, (req, res) => {
    const payMethod = req.body.payMethod;
    if (payMethod) {
      const idPayMethod = pay.length + 1;
      const newPay = {payMethod, idPayMethod};
      pay.push(newPay);
      res.json(pay);
    } else {
      res.json('incomplete data');
    }
  });
/**
 * @swagger
 *   /pay/admin:
 *    put:
 *      tags: [Pay]
 *      description: actualized pay method
 *      parameters:
 *       - name: idAdmin
 *         description: Id of admin 
 *         in: formData
 *         required: true
 *         type: string
 *       - name: indice
 *         description: Index of pay method actualized 
 *         in: formData
 *         required: true
 *         type: string
 *       - name: payMethod
 *         description: name of pay method
 *         in: formData
 *         required: true
 *         type: string
 *      responses:
 *        200:
 *          description: Success
 * 
 */
router.put('/admin', validateIndex, idAdmin, function (req, res) {
    pay[req.body.indice].payMethod = req.body.payMethod;
  res.send('actualized pay method')
})
/**
 * @swagger
 *   /pay/admin:
 *    delete:
 *      tags: [Pay]
 *      description: delete pay method
 *      parameters:
 *      - name: idAdmin
 *        description: id of admin
 *        in: formData
 *        requider: true
 *        type: string
 *      - name: indice
 *        description: index delete pay method
 *        in: formData
 *        requider: true
 *        type: string
 *      responses:
 *        200:
 *         description: Success
 */
router.delete('/admin', validateIndex, idAdmin, function (req, res) {
    pay.splice(req.body.indice,1);
  res.send('eliminated pay method');
})
module.exports = router;