const express = require('express');
const app = express();
const port = 3018;
const swaggerUI = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');
const users = require('./routes/users');
const productos = require('./routes/productos');
const payAdmin = require('./routes/payAdmin');
const orders = require('./routes/orders');

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'API de restaurante',
      version: '1.0.0'
    }
  },
  apis: ['./routes/users.js','./routes/productos.js', './routes/orders.js', './routes/payAdmin.js'],
};

const swaggerDocs = swaggerJSDoc(swaggerOptions);

app.use(express.urlencoded({ extended: true }));
app.use(express.json());


app.use('/api-docs',
  swaggerUI.serve,
  swaggerUI.setup(swaggerDocs));

//rutas
app.use('/users', users);

app.use('/productos', productos);

app.use('/orders', orders);

app.use('/pay', payAdmin);


app.listen(port, () => {
  console.log(`Listening on port http://localhost:${port}`);
});