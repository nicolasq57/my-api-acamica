const productos = require('../models/productos')

function validateIndex(req, res, next){
    if(req.body.indice >= 0 && req.body.indice < productos.length){
        next()
    }else{
        return res.status(500).json({"mensaje": " indice invalido"})
    }
    
}
module.exports = validateIndex