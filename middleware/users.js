 const users = require('../models/users');

function validateIndex(req, res, next){
    if(req.body.indice >= 0 && req.body.indice < users.length){
        next()
    }else{
        return res.status(500).json({"mensaje": "indice invalido"})
    }
}
    
module.exports = validateIndex 