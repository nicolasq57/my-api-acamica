const orders = require('../models/orders')

function validateIndex(req, res, next){
    if(req.body.indice >= 0 && req.body.indice < orders.length){
        next()
    }else{
        return res.status(500).json({"mensaje": "invalido"})
    }
    
}
module.exports = validateIndex