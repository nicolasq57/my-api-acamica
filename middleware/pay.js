const payMethod = require('../models/pay')

function validateIndex(req, res, next){
    if(req.body.indice >= 0 && req.body.indice < payMethod.length){
        next()
    }else{
        return res.status(500).json({"mensaje": "invalido"})
    }
    
}
module.exports = validateIndex