let users = require('../models/users');

 const searchEmail = (email) => {
    let emailExist = users.find(user => user.email == email);
    if (emailExist) {
        return true;
    }else{
        return false;
    }
}

const newUser = (newUser) => {
     if (searchEmail(newUser.email)){
         return "email invalid"
     } else { 
      let nUser = {
          "id": users.length + 1,
          "isAdmin": false,
          "loggedIn": false,
          "username": newUser.username,
          "password": newUser.password,
          "email": newUser.email,
          "address": newUser.address,
          "phone": newUser.phone,
          "name": newUser.name,
          "lastname": newUser.lastname,
      }
      users.push(nUser);
       return "user created" 
}
}

const login = (username, password) => {
    const userLogged = users.find(user => (user.username == username || user.email == username) && user.password == password);
    
    if (userLogged) {
        const index = users.findIndex(user => (user.username == username || user.email == username) && user.password == password);
    users[index].loggedIn = true;
        return "user logged";
    }else{
    return "invalid user";
    }
}



exports.login = login;
exports.newUser = newUser;
exports.searchEmail = searchEmail; 