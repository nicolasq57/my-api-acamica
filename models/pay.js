const payMethod = [
    {
        "idPayMethod": "1",
        "payMethod": "cash"
    },
    {
        "idPayMethod": "2",
        "payMethod": "credit card"
    }
];

module.exports = payMethod;